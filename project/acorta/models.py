from django.db import models

# Create your models here.
class Urls(models.Model):
    #url acortada, añadimos unique parano repetir urls tanto acortadas copmo no
    short=models.CharField(max_length=200, unique= True)
    #url original
    url=models.URLField(unique=True)


    def __str__(self):
        return self.url