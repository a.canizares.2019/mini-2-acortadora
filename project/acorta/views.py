from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .models import Urls

# Create your views here.
@csrf_exempt
def get_parameters(request):
    url = request.POST.get('url')
    short = request.POST.get('short')
    if not url.startswith('http://') and not url.startswith('https://'):
        url = "https://" + url
    return url, short

@csrf_exempt
def redirect_url(request,key):
    port = request.META['SERVER_PORT']
    short = "http://localhost:" + str(port) + "/" + key
    try:
        url=Urls.objects.get(short=short)
    except Urls.DoesNotExist:
        raise Http404("URL not found")
    return HttpResponseRedirect(url.url)

#manejamos solocitudes de Get y Post
@csrf_exempt
def index(request):
    if request.method == 'POST':
        url, short =get_parameters(request)

        port = request.META['SERVER_PORT']
        shorted_url = "http://localhost:" + str(port) +"/"
        #si no esta acortada
        if not short:
            shorted_url += str(Urls.objects.count() + 1)
        else:
            shorted_url += short

        Urls.objects.create(url=url, short=shorted_url)

    content_list = Urls.objects.all()
    return render( request, 'index.html',{'content_list': content_list})